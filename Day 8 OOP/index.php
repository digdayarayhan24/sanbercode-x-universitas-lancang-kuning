<?php
require_once "animal.php";
require_once "frog.php";
require_once "apes.php";

$animal = new Animal("Banteng");

echo "Name : " . $animal->name . "<br>";
echo "Legs : " . $animal->legs . "<br>";
echo "Cold Blooded : " . $animal->cold_blooded . "<br> <br>";

$kodok = new Frog("Buduk");

echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump() . "<br>";

$sungokong = new Apes("Kera Sakti");

echo "Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell() . "<br>";

?>