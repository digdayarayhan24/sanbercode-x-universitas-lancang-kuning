<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <!-- Form Sign Up -->
    <form action="/welcome" method="post">
        @csrf
        <!-- Input First Name -->
        <label for="first_name">First name:</label> <br><br>
        <input type="text" name="first_name" id="first_name"> <br><br>
        
        <!-- Input Last Name -->
        <label for="last_name">Last name:</label> <br><br>
        <input type="text" name="last_name" id="last_name"> <br><br>
        
        <!-- Input Gender -->
        <label for="gender">Gender:</label> <br><br>
        <input type="radio" name="gender" value="Male" id="gender"> Male <br>
        <input type="radio" name="gender" value="Female" id="gender"> Female <br>
        <input type="radio" name="gender" value="Other" id="gender"> Other <br><br>
        
        <!-- Input Nation -->
        <label for="nationally">Nationally:</label> <br><br>
        <select name="nationally" id="nationally">
            <option value="Indonesian">Indonesian</option>
            <option value="American">American</option>
            <option value="Australian">Australian</option>
        </select>
        <br><br>
        
        <!-- Input Language Spoken -->
        <label for="language_spoken">Language spoken:</label> <br><br>
        <input type="checkbox" name="language_spoken" value="Bahasa Indonesia" id="language_spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="language_spoken" value="English" id="language_spoken"> English <br>
        <input type="checkbox" name="language_spoken" value="Other" id="language_spoken"> Other <br><br>
        
        <!-- Input Bio -->
        <label for="bio">Bio:</label> <br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
        <input type="reset" value="Reset">

    </form>
</body>
</html>